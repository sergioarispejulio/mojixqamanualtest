﻿using ManualTestingMojix.src.code.page;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManualTestingMojix.src.code.test
{

    [TestClass]
    public class ProjectTest : TestBase
    {

        MenuSection menuSection = new MenuSection();
        TasksPage tasksPage = new TasksPage();
        string nameProject1 = "Prueba 1";
        string nameProject2 = "Prueba 2";
        string nameTask1 = "ASDF";


        [TestMethod]
        public void Test1_CreateProject()
        {
            menuSection.SetNameProjectButton(nameProject1);
            menuSection.addNewProject.Click();
            menuSection.nameNewProject.SetText(nameProject1);
            menuSection.confirmNewProject.Click();

            bool aux = menuSection.IsProjectDisplayed();
            Assert.IsTrue(aux, "ERROR !! Project wasn't create");
        }

        [TestMethod]
        public void Test2_EditNameProject()
        {
            menuSection.SetNameProjectButton(nameProject1);
            menuSection.optionsProject.Click();
            menuSection.selectProject.Click();
            menuSection.editOptionProject.Click();
            menuSection.nameEditProject.SetText(nameProject2);
            menuSection.confirmEditProject.Click();

            menuSection.SetNameProjectButton(nameProject2);

            bool aux = menuSection.IsProjectDisplayed();
            Assert.IsTrue(aux, "ERROR !! Project wasn't update");
        }


        [TestMethod]
        public void Test3_CreateTaskProject()
        {
            menuSection.SetNameProjectButton(nameProject2);
            menuSection.optionsProject.Click();
            tasksPage.taskTextBox.SetText(nameTask1);
            tasksPage.createTask.Click();

            bool aux = tasksPage.ExistTask(nameTask1);
            Assert.IsTrue(aux, "ERROR !! Task won't created");
        }

        [TestMethod]
        public void Test4_DeleteProject()
        {
            menuSection.SetNameProjectButton(nameProject2);
            menuSection.optionsProject.Click();
            menuSection.selectProject.Click();
            menuSection.deleteOptionProject.Click();
            menuSection.AcceptAlert();

            Thread.Sleep(1000);

            bool aux = menuSection.IsProjectDisplayed();
            Assert.IsFalse(aux, "ERROR !! Project still active");
        }


    }
}
