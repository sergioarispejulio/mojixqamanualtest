﻿using ManualTestingMojix.src.code.page;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManualTestingMojix.src.code.test
{
    [TestClass]
    public class TestBase
    {

        MainPage mainPage = new MainPage();
        LoginSection loginSection = new LoginSection();
        LogoutSection logoutSection = new LogoutSection();

        [TestInitialize]
        public void OpenBrowser()
        {
            session.Session.Instance().GetBrowser().Navigate().GoToUrl("http://todo.ly/");
            login();
        }

        [TestCleanup]
        public void CloseBrowser()
        {
            logout();
            session.Session.Instance().CloseBrowser();
        }


        private void login()
        {
            mainPage.loginButton.Click();
            loginSection.emailTxtBox.SetText("asdf@asdf.com");
            loginSection.pwdTxtBox.SetText("1234567890");
            loginSection.loginButton.Click();
        }

        private void logout()
        {
            logoutSection.logoutButton.Click();
        }

    }
}
