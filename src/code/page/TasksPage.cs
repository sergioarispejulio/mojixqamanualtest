﻿using ManualTestingMojix.src.code.control;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManualTestingMojix.src.code.page
{
    public class TasksPage
    {
        public TextArea taskTextBox = new TextArea(By.Id("NewItemContentInput"));
        public Button createTask = new Button(By.Id("NewItemAddButton"));



        public bool ExistTask(string nameTask)
        {
            bool result;
            Button task = new Button(By.XPath("//div[contains(@id,'ItemListPlaceHolder')]/..//td[@class='ItemContent']/..//div[contains(.,'"+ nameTask +"')]"));

            if (task.IsControlDisplayed())
            { result = true; }
            else { result = false; }    

            return result;
        }
    }
}
