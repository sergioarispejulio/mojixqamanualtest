﻿using ManualTestingMojix.src.code.control;
using ManualTestingMojix.src.code.session;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Collections.Specialized.BitVector32;

namespace ManualTestingMojix.src.code.page
{
    public class MenuSection
    {

        public Button optionsProject = null;
        public Button selectProject = null;
        public Button editOptionProject = new Button(By.XPath("//ul[contains(@id,'projectContextMenu')]/li[@class='edit']"));
        public Button deleteOptionProject = new Button(By.XPath("//ul[contains(@id,'projectContextMenu')]/li[@class='delete']"));


        public Button addNewProject = new Button(By.XPath("//div[contains(@class,'AddProjectLiDiv')]/..//td[text()='Add New Project']/parent::tr"));
        public TextBox nameNewProject = new TextBox(By.Id("NewProjNameInput"));
        public TextBox confirmNewProject = new TextBox(By.Id("NewProjNameButton"));


        public TextBox nameEditProject = new TextBox(By.XPath("//div[contains(@id,'ProjectListPlaceHolder')]/ul/li/..//input[contains(@id,\"ItemEditTextbox\")]"));
        public TextBox confirmEditProject = new TextBox(By.XPath("//div[contains(@id,'ProjectListPlaceHolder')]/ul/li/..//img[@src='/Images/save.png']"));
        public TextBox cancelEditProject = new TextBox(By.XPath("//div[contains(@id,'ProjectListPlaceHolder')]/ul/li/..//img[@src='/Images/delete.png']"));




        public void SetNameProjectButton(string nameProject)
        {
            optionsProject = new Button(By.XPath("//div[contains(@id,'ProjectListPlaceHolder')]/ul/li/..//td[text()='"+ nameProject + "']/parent::tr/..//img[@src='/Images/dropdown.png']/parent::div/parent::td/parent::tr"));
            selectProject = new Button(By.XPath("//div[contains(@id,'ProjectListPlaceHolder')]/ul/li/..//td[text()='" + nameProject + "']/parent::tr/..//img[@src='/Images/dropdown.png']"));
        }

        public bool IsProjectDisplayed()
        {
            return optionsProject.IsControlDisplayed();
        }

        public void AcceptAlert()
        {
            IAlert alert = Session.Instance().GetBrowser().SwitchTo().Alert();
            alert.Accept();
        }

    }
}
